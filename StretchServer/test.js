var Data = require('./data');

var calendar = Data('calendar', { hasMany:'day' });
var day =  Data('day');

var g = calendar.insert({
    name: 'Guitar'
});

var d1 = day.insert({ date:'monday' }, g);
var d2 = day.insert({ date:'tuesday' }, g);

console.log(JSON.stringify(calendar.res(g)));

