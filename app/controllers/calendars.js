import Controller from '@ember/controller';
import { sort } from '@ember/object/computed';

export default Controller.extend({
    init() {
        this._super(...arguments);
        this.calendarSorting = ['count:desc'];
    },
    sortedCalendars: sort('model', 'calendarSorting')
});
