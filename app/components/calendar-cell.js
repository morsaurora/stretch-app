import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({

    tagName: 'td',

    classNameBindings: ['notCurrent','hasValue'],

    notCurrent: computed('day.currMonth', function(){
        return !this.get('day.currMonth');
    }),

    hasValue: computed('day.value', function(){
        return this.get('day.value');
    }),

    click() {
        var date = this.get('day.date');
        if(this.get('day.currMonth') && moment().isSameOrAfter(date)) {
            var value = !this.get('day.value');
            this.set('day.value', value);
            this.get('markDay')(date, value);
        }
    }

});
