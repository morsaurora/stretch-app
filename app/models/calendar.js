import DS from 'ember-data';

export default DS.Model.extend({

    name: DS.attr('string'),
    notes: DS.attr('string', { defaultValue: '' }),
    days: DS.hasMany('day'),
    count: 0,
    counter: Ember.observer('days.@each.value', function() {
        this.get('days').then( days => {
            var count = 0;
            var day = moment();
            var ds = days.filterBy('date', day.format('YYYY-MM--DD'));

            while(ds.length === 1 && ds[0].get('value')) {
                count ++;
                day.subtract(1,'d');
                ds = days.filterBy('date', day.format('YYYY-MM--DD'));
            }
            this.set('count',count);
        })
    })

});
